package Exception;

public class EditingWithoutPermissionException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EditingWithoutPermissionException(String message) {
		super(message);
	}
}
