package Exception;

public class CantFindMemberException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CantFindMemberException(String message) {
		super(message);
	}
}
