package Exception;

public class CantFindAdminException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CantFindAdminException(String message) {
		super(message);
	}
}
