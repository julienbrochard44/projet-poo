package poo;

/**
 * VoluntaryTask class
 * @author Mohammed Ba Ragaa
 * 
 */
public class VoluntaryTask extends Task {

	
	public VoluntaryTask(Service service, Member beneficiary, double duration, int nbStaff) throws Exception {
		super(service, beneficiary, duration, nbStaff);
	}
	/**
	 * Defines the abstract method taskPrice in the class Task
	 * @return double = 0 for the voluntary tasks
	 */
	public int taskPrice() {
		return 0;
	}
	
	

}
