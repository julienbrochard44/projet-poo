package poo;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * CmdInterface class
 * @author Julien Brochard
 * 
 */
public class CmdInterface {
	private ArrayList<Member> members;
	private ArrayList<Admin> admins;
	private ArrayList<Network> networks;
	private ArrayList<Service> services;
	private ArrayList<Task> tasks;
	private Scanner scanner;
	private String current;
	private String separator;
	SocialClass C1;
	SocialClass C2;
	SocialClass C3;
	
	public CmdInterface() {
		this.members = new ArrayList<Member>();
		this.admins = new ArrayList<Admin>();
		this.networks = new ArrayList<Network>();
		this.services = new ArrayList<Service>();
		this.tasks = new ArrayList<Task>();
		this.current = "menu";
		this.scanner = new Scanner(System.in);
		this.separator = "*------------------------------------------*";
		
		C1 = new FullClass();
		C2 = new HalfClass();
		C3 = new ZeroClass();
		
		try {
			this.show();
		}
		catch(Exception e) {
			System.out.println("CmdInterface.show(): " + e);
		}
	}
	
	
	public void show() throws Exception {
		switch(this.current) {
			case "menu":
				this.showMenu(); 
				break;
			case "manage":
				this.showManage();				
				break;
			case "showServer":
				this.showServer();
				break;
			case "addAdmin":
				this.addAdmin();
				break;
			case "addMember":
				this.addMember();
				break;
			case "addService":
				this.addService();
				break;
			case "addTask":
				this.addTask();
				break;
			case "manageNetwork":
				this.showManageNetwork();
				break;
			default:
				throw new Exception("Current window is unknown");
		}
		this.show();
	}
	
	public void showMenu() {
		System.out.println(this.separator);
		System.out.println("Menu:");
		System.out.println("1: Manage");
		System.out.println("2: Show server");
		System.out.println("3: add Tests");
		switch(this.scanNextInt()) {
			case 1:
				this.current = "manage";
				break;
			case 2:
				this.current = "showServer";
				break;
			case 3:
				this.addTests();
				break;
		}
	}
	
	public void showManage() {
		System.out.println(this.separator);
		System.out.println("Manage:");
		System.out.println("1: Manage Network");
		System.out.println("2: addAdmin");
		System.out.println("3: addMember");
		System.out.println("4: addSerice");
		System.out.println("5: addTask");
		System.out.println("0: return to Menu");
		switch(this.scanNextInt()) {
			case 1:
				this.current = "manageNetwork";
				break;
			case 2:
				this.current = "addAdmin";
				break;
			case 3:
				this.current = "addMember";
				break;
			case 4:
				this.current = "addService";
				break;
			case 5:
				this.current = "addTask";
				break;
			case 0:
				this.current = "menu";
				break;
		}
	}
	
	public void showServer() {
		System.out.println(this.separator);
		System.out.println("Server Admins:");
		for(Admin a : this.admins) {
			System.out.println(" - " + a.toString());
		}
		System.out.println("\nServer Members:");
		for(Member m : this.members) {
			System.out.println(" - " + m.toString());
		}
		System.out.println("\nServer Networks:");
		for(int i = 0; i < this.networks.size(); i++) {
			System.out.println(" - Network " + (i+1) + ":");
			System.out.println(this.networks.get(i).toString("     "));
		}
		System.out.println("Server Services:");
		for(Service s : this.services) {
			System.out.println(" - " + s.toString());
		}
		System.out.println("\nServer Tasks:");
		for(Task t : this.tasks) {
			System.out.println(" - " + t.toString());
		}
		System.out.println("\nServer Archives:");
		for(int i = 0; i < this.networks.size(); i++) {
			System.out.println(" - " + (i+1) + " " + this.networks.get(i).archiveToString("    "));
		}
		System.out.println("\n0: return to menu");
		int input = this.scanNextInt();
		if(input == 0) {
			this.current = "menu";
		}
	}
	
	private void addAdmin() {
		SocialClass sc = this.askSocialClass();
		System.out.println("Selected: " + sc.toString());
		System.out.println("Enter the admin's name");
		String name = this.scanNextString();
		this.newAdmin(name, sc);
		this.current = "menu";
	}
	
	private void addMember() {
		SocialClass sc = this.askSocialClass();
		System.out.println("Selected: " + sc.toString());
		System.out.println("Enter the member's name");
		String name = this.scanNextString();
		this.members.add(new Member(name, sc));
		this.current = "menu";
	}
	
	private void addService() throws Exception {
		System.out.println("Enter the service's name");
		String stringInput = this.scanNextString();
		System.out.println("Enter the service's cost (>= 0)");
		int intInput = this.scanNextInt();
		this.services.add(new Service(stringInput, intInput));
		this.current = "menu";
	}
	
	private void addTask() throws Exception {
		System.out.println("Add Task:");
		System.out.println("1: addPaidTask");
		System.out.println("2: addVoluntaryTask");
		System.out.println("0: return to menu");
		int intInput = this.scanNextInt();
		switch(intInput) {
			case 1:
				this.addPaidTask();
				break;
			case 2:
				this.addVoluntaryTask();
				break;
			case 0:
				break;
			default:
				this.addTask();
				break;
		}
		this.current = "menu";
	}
	
	private void showManageNetwork() throws Exception {
		System.out.println("Manage Network");
		for(int i = 0; i < this.admins.size(); i++) {
			System.out.println(i+1 + ": " +admins.get(i).toString());
		}
		System.out.println("0: return to menu");
		int intInput = this.scanNextInt();
		if(intInput == 0) {
			this.current = "menu";
		}
		else if(intInput <= this.admins.size()) {
			this.manageNetwork(this.admins.get(intInput-1));
		}
	}
	
	private void manageNetwork(Admin a) throws Exception {
		System.out.println(a.toString());
		System.out.println("1: Admin Join");
		System.out.println("2: Member Join");
		System.out.println("3: Admin Remove");
		System.out.println("4: Member Remove");
		System.out.println("5: Validate Task");
		System.out.println("0: return to menu");
		switch(this.scanNextInt()) {
			case 1:
				this.adminJoin(a);
				break;
			case 2:
				this.memberJoin(a);
				break;
			case 3:
				this.adminRemove(a);
				break;
			case 4:
				this.memberRemove(a);
				break;
			case 5:
				this.validateTask(a);
				break;
			case 0:
				break;
			default:
				this.manageNetwork(a);
				break;
		}
		this.current = "menu";
	}
	
	private void adminJoin(Admin a) throws Exception {
		System.out.println("Make an admin join the network");
		for(int i = 0; i < this.admins.size(); i++) {
			// the comparison works because it's the same pointer
			if(this.admins.get(i).getNetwork() != a.getNetwork()){
				System.out.println(i+1 + ": " + this.admins.get(i).toString());
			}
		}
		System.out.println("0: return to menu");
		int intInput = this.scanNextInt();
		if(intInput == 0) {
			this.current = "menu";
		}
		else if(intInput <= this.admins.size() && this.admins.get(intInput-1).getNetwork() != a.getNetwork()) {
			a.addAdmin(this.admins.get(intInput-1));
			this.current = "menu";
		}
		else {
			this.adminJoin(a);
		}
	}
	
	private void memberJoin(Admin a) throws Exception {
		System.out.println("Make a member join the network");
		
		for(int j = 0; j < this.members.size(); j++) {
			if(!a.getNetwork().hasMember(this.members.get(j))){
				System.out.println(j+1 + ": " + this.members.get(j).toString());
			}
		}
		System.out.println("0: return to menu");
		int intInput = this.scanNextInt();
		if(intInput == 0) {
			this.current = "menu";
		}
		else if(intInput <= this.members.size() && !a.getNetwork().hasMember(this.members.get(intInput-1))) {
			a.addMember(this.members.get(intInput-1));
			this.current = "menu";
		}
		else {
			this.memberJoin(a);
		}
	}
	
	private void adminRemove(Admin a) {
		Admin rem = this.askAdminFromNetwork(a.getNetwork());
		
		Network n = a.removeAdmin(rem);
		if(n != null) {
			this.networks.add(n);
		}
	}
	
	private void memberRemove(Admin a) {
		Member rem = this.askMemberFromNetwork(a.getNetwork());
		a.removeMember(rem);
	}
	
	private void validateTask(Admin a) {
		a.validateTask(this.askTask());
	}
	
	private Task askTask() {
		System.out.println("Choose a task");
		for(int i = 0; i < this.tasks.size(); i++) {
			System.out.println(i+1 + ": " + this.tasks.get(i).toString());
		}
		
		int intInput = this.scanNextInt();
		if(intInput >= 1 && intInput <= this.tasks.size()) {
			return this.tasks.get(intInput-1);
		}
		
		return this.askTask();
	}
	
	private void addPaidTask() throws Exception {
		Service s = this.askService();
		Member m = this.askMember();
		System.out.println("Enter the duration (>0)");
		Double d = this.scanNextDouble();
		System.out.println("Enter the number of staff (>0 integer)");
		int nb = this.scanNextInt();
		this.tasks.add(new PaidTask(s, m, d, nb));
		this.current = "menu";
	}
	
	private void addVoluntaryTask() throws Exception {
		Service s = this.askService();
		Member m = this.askMember();
		System.out.println("Enter the duration (>0)");
		Double d = this.scanNextDouble();
		System.out.println("Enter the number of staff (>0 integer)");
		int nb = this.scanNextInt();
		this.tasks.add(new VoluntaryTask(s, m, d, nb));
		this.current = "menu";
	}
	
	private Service askService() {
		System.out.println("Choose a service");
		for(int i = 0; i < this.services.size(); i++) {
			System.out.println(i+1 + ": " + this.services.get(i).toString());
		}
		
		int intInput = this.scanNextInt();
		if(intInput >= 1 && intInput <= this.services.size()) {
			return this.services.get(intInput-1);
		}
		
		return this.askService();
	}
	
	private Member askMember() {
		System.out.println("Choose a member");
		for(int i = 0; i < this.members.size(); i++) {
			System.out.println(i+1 + ": " + this.members.get(i).toString());
		}
		
		int intInput = this.scanNextInt();
		if(intInput >= 1 && intInput <= this.members.size()) {
			return this.members.get(intInput-1);
		}
		
		return this.askMember();
	}
	
	private Member askMemberFromNetwork(Network n) {
		System.out.println("Choose an Admin to remove");
		for(int i = 0; i < n.getMembers().size(); i++) {
			System.out.println(i+1 + ": " + n.getMember(i).toString());
		}
		
		int intInput = this.scanNextInt();
		if(intInput >= 1 && intInput <= n.getMembers().size()) {
			return n.getMember(intInput-1);
		}
		
		return this.askMemberFromNetwork(n);
	}
	
	private Admin askAdminFromNetwork(Network n) {
		System.out.println("Choose an Admin to remove");
		for(int i = 0; i < n.getAdmins().size(); i++) {
			System.out.println(i+1 + ": " + n.getAdmin(i).toString());
		}
		
		int intInput = this.scanNextInt();
		if(intInput >= 1 && intInput <= n.getAdmins().size()) {
			return n.getAdmin(intInput-1);
		}
		
		return this.askAdminFromNetwork(n);
	}
	
	private SocialClass askSocialClass() {
		System.out.println("Select the social class");
		System.out.println("1: FullSize");
		System.out.println("2: HalfClass");
		System.out.println("3: ZeroClass");
		switch(this.scanNextInt()) {
			case 1:
				return this.C1;
			case 2:
				return this.C2;
			case 3:
				return this.C3;
			default:
				return this.askSocialClass();
		}
	}
	
	private void newAdmin(String name, SocialClass sc) {
		this.admins.add(new Admin(name, sc));
		this.networks.add(this.admins.get(this.admins.size()-1).getNetwork());
		this.toString();
	}
	
	private void addTests() {
		this.newAdmin("Paul", C1);
		this.newAdmin("Pierre", C2);
		this.newAdmin("Patrick", C3);
		
		this.members.add(new Member("O", C1));
		this.members.add(new Member("Olivier", C1));
		this.members.add(new Member("Olivie", C2));
		this.members.add(new Member("Olivia", C2));
		this.members.add(new Member("Olive", C3));
		this.members.add(new Member("Orange", C3));
		
		this.admins.get(2).addMember(this.members.get(5));
		this.admins.get(2).addMember(this.members.get(0));
		this.admins.get(0).addAdmin(this.admins.get(1));
		this.admins.get(1).addMember(this.members.get(2));
		this.admins.get(0).addMember(this.members.get(3));
		
		try {
			this.services.add(new Service("Jardining", 10));
			this.services.add(new Service("House cleaning", 20));
			this.services.add(new Service("Dog walk", 30));
			this.services.add(new Service("Phone fixing", 40));
			this.services.add(new Service("Car fixing", 22));
		}
		catch(Exception e) {
			System.out.println("CmdInterface.addTests " + e);
		}
		
		for(Member m : this.members) {
			m.addService(this.services.get( (int)(Math.random()*(this.services.size()-1))) );
		}
		for(Member m : this.members) {
			m.addService(this.services.get( (int)(Math.random()*(this.services.size()-1))) );
		}
		
		for(int i = 0; i < 5; i++) {
			try {
				this.tasks.add(new PaidTask( this.services.get((int)(Math.random()*(this.services.size()))) , this.members.get((int)(Math.random()*(this.members.size()))) , Math.round((Math.random()*(5)+1)*100)/100 , (int)(Math.random()*(3)+1) ));
				this.tasks.add(new VoluntaryTask( this.services.get((int)(Math.random()*(this.services.size()))) , this.members.get((int)(Math.random()*(this.members.size()))) , Math.round((Math.random()*(5)+1)*100)/100 , (int)(Math.random()*(3)+1) ));
			}
			catch(Exception e) {
				System.out.println("CmdInterface.addTests " + e);
			}
		}
		
		for(Task t : this.tasks) {
			this.admins.get( (int)(Math.random()*(this.admins.size()))).validateTask(t);
		}
	}
	
	
	
	private int scanNextInt() {
		try {
			return this.scanner.nextInt();
		}
		catch (Exception e) {
			this.scanner.next();
			return this.scanNextInt();
		}
	}
	
	private Double scanNextDouble() {
		try {
			return this.scanner.nextDouble();
		}
		catch (Exception e) {
			this.scanner.next();
			return this.scanNextDouble();
		}
	}
	
	private String scanNextString() {
		try {
			return this.scanner.next();
		}
		catch (Exception e) {
			this.scanner.next();
			return this.scanNextString();
		}
	}
	
}
