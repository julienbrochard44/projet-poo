package poo;
/**
 * 
 * ZeroClass class
 * @author Mohamed Ba Ragaa
 * 
 */
public class ZeroClass implements SocialClass {
	/**
	 * Get the factor for the zero class 
	 * @return double = 0
	 */
	public double getFractoin() {
		return 0;
	}
	
	public String toString() {
		return("ZeroClass");
	}


}