package poo;
/**
 * Result enum
 * @author mohammed BA Ragaa
 * this enum is an attribute in the task report
 */

public enum Result {
	successed,
	failed;

}
