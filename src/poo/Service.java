package poo;
import java.util.ArrayList;

import Exception.NegativeNumberException;

/**
 * Service class
 * @author Mohamed Ba Ragaa
 */
public class Service {
	private String serviceName;
	private int costPerHour;
	
	public Service(String serviceName,int costPerHour) throws Exception {
		this.serviceName = serviceName;
		this.setCostPerHour(costPerHour);	

	}
	/**
	 * Gets the cost per hour of a service
	 * @return cost per Hour(in a double number)
	 */
	public int getCostPerHour() {

		return costPerHour;
	}
	/**
	 * Sets the Hourly cost of a service
	 * @param costPerHour ( an integer representing the tokens needed for a Service each hour).
	 * @throws Exception (unacceptable negative value )
	 * @return void
	 */
	private void setCostPerHour(int costPerHour) throws Exception {
		if(costPerHour<0) {
			throw new NegativeNumberException("Service.setCostPerHour(int), '" + costPerHour + "' is a negative number");		
		} 
		else{
			this.costPerHour = costPerHour;
			
		}
	}
	/**
	 * Gets the service's name
	 * @return the Service's name
	 */
	public String getServiceName() {
		return this.serviceName;
	}
	
	/**
	 * Gets the String describing a Service
	 * @return String
	 */
	public String toString() {
		return "Service '"+this.getServiceName()+"' Costs '"+this.getCostPerHour()+"' tokens per hour";
	}
}
