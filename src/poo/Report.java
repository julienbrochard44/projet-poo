package poo;

/**
 * Report class
 * @author mohammed BA Ragaa
 * 
 */
public class Report {
	private String adminName;
	private Task task;
	private Result result; 
	
	
	Report(String adminName ,Task task, Result result ){
		this.setAdminName(adminName);
		this.setTask(task);
		this.setResult(result);
	}


	public Task getTask() {
		return task;
	}


	public void setTask(Task task) {
		this.task = task;
	}


	


	public Result getResult() {
		return result;
	}


	public void setResult(Result result) {
		this.result = result;
	}


	public String getAdminName() {
		return adminName;
	}


	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}
}
