package poo;

/**
 * PaidTask class
 * @author Mohammed Ba Ragaa
 */
public class PaidTask extends Task {

	public PaidTask(Service service,Member beneficiary, double duration, int nbStaff) throws Exception {
		super(service, beneficiary, duration, nbStaff);
	}
	/**
	 *  returns the price of non-zero  Task
	 *  @return the taskPeice as a redefinition of the abstract method
	 */
	
	public int taskPrice() {
		return (int)((this.getService().getCostPerHour() * this.getDuration() * this.getNbStaff()) * this.getBeneficiary().getSocialClass().getFractoin()) ;
	}

}
