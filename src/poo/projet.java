package poo;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

public class projet {

	public static void main(String[] args) {
		
		 System.out.println("Tests sans interface" ); 
		// TODO Auto-generated method stub
		SocialClass C1 = new FullClass();
		SocialClass C2 = new HalfClass();
		SocialClass C3 = new ZeroClass();
		
		Member m1 = new Member("John",C1);
		Member m2 = new Member("Pierre",C2);
		Member m3 = new Member("Jean",C3);
		Member m4 = new Member("Paul",C1);
		Member m5 = new Member("Paula",C1);
		Admin a1 = new Admin("Johnny",C1);
		Admin a2 = new Admin("Johnna",C1);
		Admin a3 = new Admin("Tom",C3);
		
		
		
		
		a1.addMember(m1);
		a1.addMember(m2);
		a1.addMember(m3);
		a1.addMember(m4);
		a3.addMember(m1);
		a3.addMember(m2);
		a3.addMember(m3);
		a3.addMember(m4);
		a3.addMember(m5);
		
		System.out.println("a1.removeMember(\"test\");");
		a1.removeMember("test");
		a1.removeMember(m2);
		System.out.println("");
		a1.removeMember("John");
		a2.removeMember(m3);
		System.out.println("Because Jean is added to the Network of admin 1 not 2 ");
		
		
		
		
		try {
			//a1.getNetwork().addMember(m1);
			//Creating Tasks
			Service s1 = new Service("Jardining", 10);
			Service s2 = new Service("House cleaning", 20);
			Service s3 = new Service("Dog walk", 30);
			Service s4 = new Service ("Phone fixing", 40);
			Service s5 = new Service("Car fixing", 22);
			Service s6 = new Service("Jardinage", 16);
			Service s7 = new Service("Jardinage", 15);
			Service s8 = new Service("Jardinage", 5);
			
		//First list of Services
			ArrayList<Service>  services1 = new ArrayList<Service> ();
			services1.add(s1);
			services1.add(s2);
			services1.add(s3);
			services1.add(s4);
			services1.add(s5);
			services1.add(s6);
			services1.add(s7);
		//Second List of Services
			ArrayList<Service>  services2 = new ArrayList<Service> ();
			services2.add(s1);
			services2.add(s2);
			services2.add(s3);
			services2.add(s4);
			services2.add(s5);
			services2.add(s6);
		//Third List of Services
			ArrayList<Service>  services3 = new ArrayList<Service> ();
			services3.add(s1);
			services3.add(s2);
			services3.add(s3);
			
			
			//Implementing the services on members
			m1.addListOfServices(services1); // added a list of Services
			m2.addListOfServices(services1);
			m3.addListOfServices(services3);
			m5.addListOfServices(services1);

			m4.addService(s1);				//added Services one by one
			m4.addService(s3);
			m4.addService(s5);
			
		//the implemented exceptions of the negative values 	
			try {
				System.out.println("\n"+"Task t6 = new PaidTask(s2,m1,-2,5);");
				Task t6 = new PaidTask(s2,m1,-2,5);
			}catch(Exception e) {
				System.out.println(e);	
			}
			try {
				System.out.println("\n"+"Task t7 = new PaidTask(s2,m1,2,-5);");
				Task t7 = new PaidTask(s2,m1,2,-5);
			}catch(Exception e) {
				System.out.println(e);	
			}
			try {
				System.out.println("\n"+"Service s9 = new Service(\"Jardinage\", -2);");
				Service s9 = new Service("Jardinage", -2);

			}catch(Exception e) {
				System.out.println(e);	
			}
			
			Task t1 = new PaidTask(s2,m1,0.5,5);
			Task t2 = new PaidTask(s3,m2,1.2,1);
			Task t3 = new VoluntaryTask(s1,m3,1,1);			
			Task t4 = new PaidTask(s4,m2,1.2,1);
			Task t5 = new PaidTask(s8,m2,1.2,1);


			
			a1.addAdmin(a2);

			 	
			a1.validateTask(t1);
			a1.validateTask(t2);
			a2.validateTask(t3);
			a2.validateTask(t5);
			a3.validateTask(t4);
			a3.validateTask(t1);
			

			
			
		}
		catch(Exception e) {
			System.out.println("Main, " + e);
		}
		System.out.println("\n"+"Network of admin 1 et 2"+"\n");
		System.out.println(a1.networkToString(""));
		System.out.println("Network of admin 3"+"\n");
		System.out.println(a3.networkToString(""));

		System.out.println("Archive of the Network of admin 1 and 2 ");

		System.out.println(a1.archiveToString(""));
		
		System.out.println("Archive of the Network of admin 3");
		System.out.println(a3.archiveToString(""));

		try {
			System.setOut(new PrintStream(new FileOutputStream("Archive.txt")));	 //To set the standard out-put to the a new file(.txt) for example
			System.out.println(a1.archiveToString("")); // now the file will be written in the selected file 
			
		}
		catch(Exception e) {
			System.out.println("affichage en ficher"+ e);
		}
	

		
		//CmdInterface cmd = new CmdInterface();

		
	}

}
