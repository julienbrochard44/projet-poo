package poo;

/**
 * Demi class
 * @author Mohamed Ba Ragaa
 * @version 1
 */
public class HalfClass implements SocialClass{
	
	/**
	 * Get the factor for the Half class 
	 * @return double = 0.5
	 */
	public double getFractoin() {
		return 0.5;
	}
	
	public String toString() {
		return("HalfClass");
	}

}