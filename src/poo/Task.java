package poo;

import Exception.NegativeNumberException;

/**
 * Zero class
 * @author Mohammed Ba Ragaa
 * 
 */
public abstract class Task {
	private Service service;
	private Member beneficiary;
	private double duration;
	private int nbStaff;
	
	public Task(Service service, Member beneficiary, double duration, int nbStaff) throws Exception {
		this.setService(service);
		this.beneficiary = beneficiary;
		this.setDuration(duration);
		this.setNbStaff(nbStaff);
		
	}
	/**
	 * Abstract methode
	 * Get the Integer part of the price of a task as a product of : Service cost per hour * Duration in hours * number of staff on the task  ->
	 * -> * the factor of the social class of the beneficiary.
	 * @return the integer part of the price 
	 */
	public abstract int taskPrice();
	
	/**
	 * Gets the Service to be done in the Task
	 * @return the task's service.
	 */
	public Service getService() {
		return service;
	}
	/**
	 * Sets the Service to be done in the Task 
	 * @param service
	 * @return void
	 */
	public void setService(Service service) {
		this.service = service;
	}
	/**
	 * Gets the beneficiary of the Task
	 * @return  member 
	 */
	protected Member getBeneficiary(){
		return this.beneficiary;
	}
	
	/**
	 * Gets the Duration of the Task in hours
	 * @return the Task's Duration
	 */
	public double getDuration() {
		return duration;
	}
	/**
	 * 
	 * @param duration
	 * @throws Exception(unacceptable negative value )
	 * @return void
	 */
	private void setDuration(double duration) throws Exception {
		if(duration >= 0) {
			this.duration = duration;
		} else {
			throw new NegativeNumberException("Task.setDuration(double), '" + duration + "' is a negative number");
		}
	}
	/**
	 * Gets the number of Members contributing in the Task.
	 * @return the Tasks staff number.
	 */
	public int getNbStaff() {
		return nbStaff;
	}
	
	/**
	 * Sets a positive number of the staff for a task
	 * @param nbStaff
	 * @throws Exception(unacceptable negative value )
	 * @return void
	 */
	public void setNbStaff(int nbStaff) throws Exception {
		if(nbStaff<=0) {
			throw new NegativeNumberException("Service.setCostPerHour(int), '" + nbStaff + "' is a negative number");		
		}
			try {
				this.nbStaff = nbStaff;

			}
			catch(Exception e){
				System.out.println ("Task.setNbStaff(int), '" + nbStaff + "' Has to be an Inreger"+ e) ;
			
		}
	}
	
	/**
	 * Gets the String describing a Task
	 * @return String
	 */
	public String toString(){
		return this.getService().getServiceName() + "' done by '" + this.getNbStaff() + "' persons for '" + this.getBeneficiary().getName() + "' for a Duration of '" + this.getDuration() + "' hours and cost '" + this.taskPrice() + "' Tokens";
	}
}
