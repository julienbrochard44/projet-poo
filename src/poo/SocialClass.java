package poo;

/**
 * SocialClass class
 * @author Mohammed Ba Ragaa
 * 
 */
public interface SocialClass {
	/**
	 * Gets the fraction used in calculating the cost of a task depending on the SocialClass of the beneficiary. 
	 * @return  a fraction (double >=1)
	 */
	public abstract double getFractoin();
	public abstract String toString();
}
