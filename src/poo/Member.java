package poo;
import java.util.ArrayList;

/**
 * Member class
 * @author Julien Brochard
 * @version 1
 */
public class Member {
	private String name;
	private int token;
	private SocialClass socialClass;
	private ArrayList<Service> listOfServices;
	
	public Member(String name, SocialClass socialClass) {
		this.name = name;
		this.token = 500; // utiliser final ?
		this.socialClass = socialClass;
		this.listOfServices = new ArrayList<Service>();
	}

	/**
	 * Get the Member's name
	 * @return the member's name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Set the Member's name
	 * @return void
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the Member's token
	 * @return the member's token
	 */
	public int getToken() {
		return this.token;
	}

	// ------------------------------------------A SECURISER ------------------------------------------------
	protected void takeToken(int t) {
		this.token -= t;
	}
	
	protected void giveToken(int t) {
		this.token += t;
	}
	// ------------------------------------------A SECURISER ------------------------------------------------
	
	/**
	 * Get the member's description
	 * @return void
	 */
	public String toString() {
		String services = "";
		for(Service s : this.listOfServices) {
			services += s.getServiceName() + ", ";
		}
		if(services.length() > 0) { // remove the last ", "
			services = services.substring(0, services.length() - 2);
		}
		return this.name + "(" + this.getSocialClass().toString()+ "): " + this.token + " tokens <" + services + ">";
	}

	/**
	 * Get the list of the Services of which the member is capable 
	 * @return ArrayList
	 */
	public ArrayList<Service> getListOfServices() {
		return listOfServices;
	}
	/**
	 * Add a service to the list of the Services of which the member is capable .
	 * @return void
	 */
	public void addService(Service service) {
		boolean alreadyExists = false ;			//to evade adding duplicates 
		for(Service s :this.listOfServices) {
			if (s == service) {
				alreadyExists = true;
			}
		}
		if(!alreadyExists) {
			this.listOfServices.add(service);
			//System.out.println(this.name + " Says : "+"I can do '" + service.getServiceName() + "'.");
		}
	}
	
	/**
	 * Add a list of services to the list of the Services of which the member is capable .
	 * @return void
	 */
	public void addListOfServices(ArrayList<Service> list) {
		for(Service s : list) {
			this.addService(s);
		}
	}
	

	public SocialClass getSocialClass() {
		return socialClass;
	}

}
