package poo;
import java.util.ArrayList;
import java.util.HashMap;
import Exception.CantFindAdminException;
import Exception.CantFindMemberException;
import Exception.EditingWithoutPermissionException;

/**
 * Network class
 * @author Julien Brochard
 * 
 */
public class Network {
	private ArrayList<Admin> admins;
	private ArrayList<Member> members;
	private ArrayList<Report> archive;
	
	public Network(Admin admin) {
		this.admins = new ArrayList<Admin>();
		this.admins.add(admin);
		this.members = new ArrayList<Member>();
		this.archive = new ArrayList<Report>();
	}
	
	/**
	 * Get an ArrayList of all Members
	 * @return ArrayList of all Members
	 */
	public ArrayList<Member> getMembers() {
		return this.members;
	}
	
	/**
	 * Get an ArrayList of all Admins
	 * @return ArrayList of all Admins
	 */
	public ArrayList<Admin> getAdmins() {
		return this.admins;
	}
	
	/**
	 * Get an Admin identified by its id
	 * @param id   id of the Admin to get
	 * @return the Admin
	 */
	public Admin getAdmin(int id) {
		return this.admins.get(id);
	}
	
	/**
	 * Get a Member identified by its id
	 * @param id   id of the Member to get
	 * @return the Member
	 */
	public Member getMember(int id) {
		return members.get(id);
	}

	/**
	 * Find an Admin by its name and return it
	 * @param name   Name of the Admin to find
	 * @return the Admin
	 */
	public Admin findGetAdmin(String name) throws Exception {
		for(Admin a : this.admins) {
			if(a.getName() == name) {
				return a;
			}
		}
		throw new CantFindAdminException("Network.findGetAdmin(String), '" + name + "' not found");
	}
	
	/**
	 * Find a Member by its name and return it
	 * @param name   Name of the Member to find
	 * @return the Member
	 */
	public Member findGetMember(String name) throws Exception {
		for(Member m : this.members) {
			if(m.getName() == name) {
				return m;
			}
		}
		throw new CantFindMemberException("Network.findGetMember(String), '" + name + "' not found");
	}
	
	/**
	 * Find an Admin by its name and return it
	 * @param name   Name of the Admin to find
	 * @return the Admin
	 */
	public Admin findAdmin(String name) throws Exception {
		for(int i = 0; i < this.admins.size(); i++) {
			if(this.admins.get(i).getName() == name) {
				return this.admins.get(i);
			}
		}
		throw new CantFindAdminException("Network.findAdmin(String), '" + name + "' not found");
	}
	
	/**
	 * Find an Admin by its name and return its id
	 * @param name   Name of the Admin to find
	 * @return the Admin's id
	 */
	public int findAdminId(String name) throws Exception {
		for(int i = 0; i < this.admins.size(); i++) {
			if(this.admins.get(i).getName() == name) {
				return i;
			}
		}
		throw new CantFindAdminException("Network.findAdmin(String), '" + name + "' not found");
	}
	
	/**
	 * Find a Member by its name and return its id
	 * @param name   Name of the Member to find
	 * @return the Member's id
	 */
	public int findMemberId(String name) throws Exception {
		for(int i = 0; i < this.members.size(); i++) {
			if(this.members.get(i).getName() == name) {
				return i;
			}
		}
		throw new CantFindMemberException("Network.findMember(String), '" + name + "' not found");
	}
	
	public void addReportToArchive(Report report) throws EditingWithoutPermissionException {
		if(this.areAdminsEditing()) {
			this.archive.add(report);
		}
		else {
			throw new EditingWithoutPermissionException("Network.addReportToArchive(Report) not allowed, no admins are editing"); 
		}
	}
	
	public Report getReportFromArchive(int i) {
		return this.archive.get(i);
	}
	
	public String archiveToString(String spacing){
		String out = "Archive:";
		for(Report r : this.archive){
			out +=("\n" + spacing + "  Admin: " + r.getAdminName() + " '" + r.getResult() + "' the Task '" + r.getTask().toString()); 
		}
		return out;
	}
	
	/**
	 * Remove the Admin identified by its id
	 * @param id   the Admin's id
	 * @return void
	 */
	public Network removeAdmin(Admin a) throws Exception {
		if(this.areAdminsEditing()) {
			this.admins.remove(a);
			return a.newNetwork();
		}
		else {
			throw new EditingWithoutPermissionException("Network.removeAdmin(int) not allowed, no admins are editing"); 
		}
	}
	
	/**
	 * Remove the Member identified by its id
	 * @param id   the Member's id
	 * @return void
	 */
	public void removeMember(int id) throws Exception{
		if(this.areAdminsEditing()) {
			this.members.remove(id);
		}
		else {
			throw new EditingWithoutPermissionException("Network.removeMember(int) not allowed, no admins are editing"); 
		}
	}
	
	/**
	 * Add an Admin
	 * @param admin   the Admin to add
	 * @return void
	 */
	public void addAdmin(Admin admin) throws Exception {
		if(this.areAdminsEditing()) {
			this.admins.add(admin);
			admin.joinNetwork(this);
		}
		else {
			throw new EditingWithoutPermissionException("Network.addAdmin(Admin) not allowed, no admins are editing"); 
		}
	}
	
	/**
	 * Add a Member
	 * @param member   the Member to add
	 * @return void
	 */
	public void addMember(Member member) throws Exception {
		if(this.areAdminsEditing()) {
			this.members.add(member);
		}
		else {
			throw new EditingWithoutPermissionException("Network.addMember(Member) not allowed, no admins are editing"); 
		}
	}
	
	/**
	 * See if an Admin is editing
	 * @return true if an Admin is editing
	 */
	public boolean areAdminsEditing() {
		for(Admin a : this.admins) {
			if(a.isEditing()) {
				a.stopEditing();
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Make a list of all Admins and Members
	 * @return a string of all Admins and Members
	 */
	public String toString(String spacing) {
		String out = spacing + "Admins: \n";
		for(Admin a : this.admins) {
			out += spacing +  " - " + a.toString() + "\n";
		}
		out += spacing +  "Members: \n";
		for(Member m : this.members) {
			out += spacing +  " - " + m.toString() + "\n";
		}
		return out;
	}
	
	
	/**
	 * See if there is no members
	 * @return true if there is no members
	 */
	public boolean isMembersEmpty() {
		return this.members.isEmpty();
	}
	/**
	 * Verifies if the member is in the network. 
	 * @param member
	 * @return boolean
	 */
	public boolean hasMember(Member member) {
		for(Member m : this.members) {
			if(m == member) {
				return true;
			}
		}
		return false;
	}
	
}
