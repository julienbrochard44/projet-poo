package poo;

/**
 * Full class
 * @author Mohamed Ba Ragaa
 * @version 1
 */
public class FullClass implements SocialClass {
	/**
	 * Get the factor for the Full class 
	 * @return double = 1
	 */
	public double getFractoin() {
		return 1;
	}

	public String toString() {
		return("FullClass");
	}

}