package poo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Admin class
 * @author Julien Brochard
 * @version 1
 */
public class Admin extends Member {
	private Network network;
	private boolean isEditing;
	
	private HashMap<Integer,Report> record;
	
	public Admin(String name , SocialClass socialClass) {
		super(name,socialClass);
		this.isEditing = false;
		this.newNetwork();
		this.record = new HashMap<Integer,Report>();
	}
	
	/**
	 * Get the Admin's Network
	 * @return the network
	 */
	public Network getNetwork() {
		return this.network;
	}

	/**
	 * Create a new Network for this admin
	 * @return void
	 */
	public Network newNetwork() {
		this.network = new Network(this);
		return this.network;
	}
	
	/**
	 * Join an other Admin's Network
	 * @param network   the Network to join
	 * @return void
	 * @throws Exception 
	 */
	public void joinNetwork(Network network) throws Exception {
		this.removeAdmin(this);
		this.network = network;
	}

	/**
	 * Get the editing state of the Admin
	 * @return return true if the Admin is Editing
	 */
	public boolean isEditing() {
		return this.isEditing;
	}

	/**
	 * Stop the Admin editing state (turn it to false)
	 * @return void
	 */
	public void stopEditing() {
		if(this.isEditing) {
			this.isEditing = false;
		}
	}
	
	/**
	 * Add an Admin to the Network
	 * @param admin   the Admin to add
	 * @return void
	 */
	public void addAdmin(Admin admin) {
		this.isEditing = true;
		try {
			this.network.addAdmin(admin);
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.addAdmin(Admin), " + e);
		}
	}
	
	/**
	 * Remove an Admin from the Network
	 * @param id   the Admin's id
	 * @return void
	 */
	public Network removeAdmin(int id) {
		this.isEditing = true;
		try {
			return this.network.removeAdmin(this.network.getAdmin(id));
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.removeAdmin(int), " + e);
		}
		return null;
	}
	
	/**
	 * Remove an Admin from the Network
	 * @param name   the Admin's name
	 * @return void
	 */
	public Network removeAdmin(String name) {
		this.isEditing = true;
		try {
			return this.network.removeAdmin(this.network.findAdmin(name));
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.removeAdmin(String), " + e);
		}
		return null;
	}
	
	/**
	 * Remove an Admin from the Network
	 * @param member   the Admin
	 * @return void
	 */
	public Network removeAdmin(Admin admin) {
		this.isEditing = true;
		try {
			return this.network.removeAdmin(this.network.findAdmin(admin.getName()));
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.removeAdmin(Admin), " + e);
		}
		return null;
	}
	
	/**
	 * Add a Member to the Network
	 * @param member   the Member to add
	 * @return void
	 */
	public void addMember(Member member) {
		this.isEditing = true;
		try {
			this.network.addMember(member);
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.addMember(Member), " + e);
		}
	}
	
	/**
	 * Remove a Member from the Network
	 * @param id   the Member's id
	 * @return void
	 */
	public void removeMember(int id) {
		this.isEditing = true;
		try {
			this.network.removeMember(id);
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.removeMember(int), " + e);
		}
	}
	
	/**
	 * Remove a Member from the Network
	 * @param name   the Member's name
	 * @return void
	 */
	public void removeMember(String name) {
		this.isEditing = true;
		try {
			this.network.removeMember(this.network.findMemberId(name));
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.removeMember(String), " + e);
		}
	}
	
	/**
	 * Remove a Member from the Network
	 * @param member   the Member
	 * @return void
	 */
	public void removeMember(Member member) {
		this.isEditing = true;
		try {
			this.network.removeMember(this.network.findMemberId(member.getName()));
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.removeMember(Member), " + e);
		}
	}
	
	/**
	 * Make a list of all Admins and Members
	 * @return a string of all Admins and Members
	 */
	public String networkToString(String spacing) {
		return this.getNetwork().toString(spacing);
	}
	/**
	 * Tests if the task verifies the necessary conditions to be executed
	 * @param task
	 * @return void
	 */
	public void validateTask(Task task) {
		String nameOfAdmin = this.getName(); //must be initialized because the static methods do not accept the word  this
		if(this.nbOfQualified(task.getService()) >= task.getNbStaff()) {
			if(task.getBeneficiary().getToken() >= task.taskPrice()) {
				ArrayList<Member> staff = this.selectStaff(this.getAllQualified(task), task);
				executeTask(staff, task);
				this.addReportToArchive(new Report(nameOfAdmin, task, Result.successed));


			}
		}
		else {
			this.addReportToArchive(new Report(nameOfAdmin,task,Result.failed));
		}
	}
	/**
	 * Executes the validated tasks
	 * @param staff
	 * @param task
	 * @return void
	 */
	private void executeTask(ArrayList<Member> staff, Task task) {
		
		int division = task.taskPrice()/task.getNbStaff();
		task.getBeneficiary().takeToken(task.taskPrice());
		for(Member m : staff) {
			m.giveToken(division);
		}
	}
	
	/**
	 * returns the qualified staff selected to do the task
	 * @param qualified
	 * @param task
	 * @return ArrayList<Member> 
	 */
	public ArrayList<Member> selectStaff(ArrayList<Member> qualified, Task task) {
		ArrayList<Member> staff = new ArrayList<Member>();
		for(int i = 0; i < task.getNbStaff(); i++) {
			staff.add(qualified.get(i));
		}
		return qualified;
	}
	/**
	 * returns all the qualified members for the task in an arrayList
	 * @param task
	 * @return ArrayList<Members>
	 */
	public ArrayList<Member> getAllQualified(Task task) {
		ArrayList<Member> qualified = new ArrayList<Member>();
		for(Member m : this.network.getMembers()) {
			for(Service s : m.getListOfServices()){
				if(task.getService().getServiceName() == s.getServiceName()) {
					qualified.add(m);
				}
			}
		}
		return qualified;
	}
	
	/**
	 * Gets the number of the qualified  members for a service in a network  
	 * @param list of members , Service
	 * @return integer (the number of of the qualified members of a service)
	 */
	public int nbOfQualified( Service service) {  
		int nbOfQualified = 0;
		for(Member m : this.network.getMembers()) {
			for(Service s : m.getListOfServices()){
				if(service.getServiceName() == s.getServiceName()) {
					nbOfQualified++;
				}
			}
		}
		return nbOfQualified;
	}
	
	/**
	 * Adds the task validated by the 	Admin either accepted or not 
	 * @param report
	 * @return void
	 */
	private  void addReportToArchive(Report report){
		this.isEditing = true;
		try {
			this.network.addReportToArchive(report);
		}
		catch(Exception e) {
			this.stopEditing();
			System.out.println("Admin.addReportToArchive(Report), " + e);
		}
		
	}
	/**
	 * returns the Report from the archive
	 * @param the index of the Report
	 * @return
	 */
	public Report getReportFromArchive(int i) {
		return this.network.getReportFromArchive(i);
	}
	
	
	/**
	 * Returns a record of all the tasks in the network 
	 * @return String 
	 */
	
	public String archiveToString(String spacing){
		return this.network.archiveToString(spacing);
	}

}
